//#include "preferences.h"

class ButtonMatrix
{
private:
    uint8_t rows[16];
    uint8_t cols[16];
    uint8_t buttonAssignments[16][16];

    uint8_t matrixColCount()
    {
        // return sizeof(cols) / sizeof(cols[0]);
        return 16;
    }

    uint8_t matrixRowCount()
    {
        // return sizeof(rows) / sizeof(rows[0]);
        return 16;
    }

public:
    ButtonMatrix()
    {
        // for (int x = 0; x < matrixRowCount(); x++)
        // {
        //     rows[x] = x;
        // }
        // for (int x = 0; x < matrixColCount(); x++)
        // {
        //     cols[x] = x;
        // }
        // for (int row = 0; row < 16; row++)
        // {
        //     for (int col = 0; col < 16; col++)
        //     {
        //         //buttonMatrix.SetButtonAssignment(row, col, Read8BitValue(544 + (row + col * 16)));
        //         SetButtonAssignment(row, col, row * 16 + col);
        //     }
        // }
    }

    void SetButtonAssignment(uint8_t row, uint8_t col, uint8_t assignment)
    {
        buttonAssignments[col][row] = assignment;
    }

    void SetButtonAssignment(uint8_t idx, uint8_t assignment)
    {
        buttonAssignments[idx%16][idx/16] = assignment;
    }

    void SetRowPin(uint8_t rowIdx, uint8_t pin)
    {
        rows[rowIdx] = pin;
    }

    void SetColPin(uint8_t colIdx, uint8_t pin)
    {
        cols[colIdx] = pin;
    }

    uint8_t GetRowPin(uint8_t idx){
        return rows[idx];
    }

    uint8_t GetColPin(uint8_t idx){
        return cols[idx];
    }

    uint8_t GetButtonAssignment(uint8_t idx){
        return buttonAssignments[idx%16][idx/16];
    }

    void MatrixInit()
    {
        for (uint8_t x = 0; x < matrixRowCount(); x++)
        {
            if (rows[x] != 0)
            {
                pinMode(rows[x], INPUT);
            }
        }

        for (uint8_t x = 0; x < matrixColCount(); x++)
        {
            if (cols[x] != 0)
            {
                pinMode(cols[x], INPUT_PULLUP);
            }
        }
    }

    void ReportConfig()
    {
        for (int x = 0; x < matrixRowCount(); x++)
        {
            Serial.write(rows[x]);
        }
        for (int x = 0; x < matrixColCount(); x++)
        {
            Serial.write(cols[x]);
        }
        for (int col = 0; col < 16; col++)
        {
            for (int row = 0; row < 16; row++)
            {
                Serial.write(buttonAssignments[row][col]);
            }
        }
    }

    void PrintMatrixState()
    {
        // Report Matrix Button Count
        Serial.write(matrixRowCount() * matrixColCount());

        // iterate the columns
        for (uint8_t colIndex = 0; colIndex < matrixColCount(); colIndex++)
        {
            // col: set to output to low
            if (cols[colIndex] != 0)
            {
                pinMode(cols[colIndex], OUTPUT);
                digitalWrite(cols[colIndex], LOW);

                // row: interate through the rows
                for (uint8_t rowIndex = 0; rowIndex < matrixRowCount(); rowIndex++)
                {
                    if (rows[rowIndex] != 0)
                    {
                        pinMode(rows[rowIndex], INPUT_PULLUP);

                        // Report button state 0/1
                        Serial.write(digitalRead(rows[rowIndex]));

                        pinMode(rows[rowIndex], INPUT);
                    }
                }
                // disable the column
                pinMode(cols[colIndex], INPUT);
            }
        }

        // Report end of matrix report
        Serial.write(255);
    }

    void LoadConfigEEPROM()
    {
        // Get pin assignments
        // for (int i = 0; i < 16; i++)
        // {
        //     buttonMatrix.SetRowPin(i, Read8BitValue(512 + i));
        // }
        // for (int i = 0; i < 16; i++)
        // {
        //     buttonMatrix.SetColPin(i, Read8BitValue(528 + i));
        // }

        // // Get Button Assignments

        // for (int row = 0; row < 16; row++)
        // {
        //     for (int col = 0; col < 16; col++)
        //     {
        //         buttonMatrix.SetButtonAssignment(row, col, Read8BitValue(544 + (row * 16 + col)));
        //         //buttonMatrix.SetButtonAssignment(row, col, row + col * 16);
        //     }
        // }
        // Serial.println("matrix loaded");
    }
};